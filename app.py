from flask import Flask,render_template,request,redirect,url_for,flash
import sqlite3 as sql
app=Flask(__name__)

@app.route("/")
@app.route("/index")
def index():
    con=sql.connect("db_web.db")
    con.row_factory=sql.Row
    cur=con.cursor()
    cur.execute("select * from users")
    data=cur.fetchall()
    return render_template("index.html",datas=data)

@app.route("/add_person",methods=['POST','GET'])
def add_person():
    if request.method=='POST':
        firstname=request.form['firstname']
        lastname=request.form['lastname']
        email=request.form['email']
        age=request.form['age']
        con=sql.connect("db_web.db")
        cur=con.cursor()
        cur.execute("insert into users(FIRSTNAME,LASTNAME,EMAIL,AGE) values (?,?,?,?)",(firstname,lastname,email,age))
        con.commit()
        flash('Person Added','success')
        return redirect(url_for("index"))
    return render_template("add_person.html")

@app.route("/edit_person/<string:uid>",methods=['POST','GET'])
def edit_person(uid):
    if request.method=='POST':
        firstname=request.form['firstname']
        lastname=request.form['lastname']
        email=request.form['email']
        age=request.form['age']
        con=sql.connect("db_web.db")
        cur=con.cursor()
        cur.execute("update users set FIRSTNAME=?,LASTNAME=?,EMAIL=?,AGE=? where UID=?",(firstname,lastname,email,age,uid))
        con.commit()
        flash('Person Updated','success')
        return redirect(url_for("index"))
    con=sql.connect("db_web.db")
    con.row_factory=sql.Row
    cur=con.cursor()
    cur.execute("select * from users where UID=?",(uid,))
    data=cur.fetchone()
    return render_template("edit_person.html",datas=data)

@app.route("/view_person/<string:uid>",methods=['POST','GET'])
def view_person(uid):
    if request.method=='POST':
        firstname=request.form['firstname']
        lastname=request.form['lastname']
        email=request.form['email']
        age=request.form['age']
        con=sql.connect("db_web.db")
        cur=con.cursor()
        cur.execute("select * from users where UID=?",(uid,))
        con.commit()
        flash('','success')
        return redirect(url_for("index"))
    con=sql.connect("db_web.db")
    con.row_factory=sql.Row
    cur=con.cursor()
    cur.execute("select * from users where UID=?",(uid,))
    data=cur.fetchone()
    return render_template("view_person.html",datas=data)
    
@app.route("/delete_person/<string:uid>",methods=['GET'])
def delete_person(uid):
    con=sql.connect("db_web.db")
    cur=con.cursor()
    cur.execute("delete from users where UID=?",(uid,))
    con.commit()
    flash('Person Deleted','warning')
    return redirect(url_for("index"))
    
if __name__=='__main__':
    app.secret_key='admin123'
    app.run(debug=True)